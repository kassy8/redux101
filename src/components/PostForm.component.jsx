import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {newPost} from '../actions/post.actions';

class PostForm extends Component{

    constructor(props){
        super(props);
        this.state= {
            title: '',
            body: ''
        };

        //this.onChange = this.onChange.bind(this);
    }

    handleChange= (event) => {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const post = {
            title: this.state.title,
            body: this.state.body
        }
        this.props.newPost(post);
    }

    render(){
        return(
        <div>
            <h1>Add Post</h1>
            <form>
                <div>
                    <label>Title: </label><br />
                    <input type="text" name="title" onChange={this.handleChange} value={this.state.title} />
                </div>
                <br />
                <div>
                    <label>Body: </label><br />
                    <textarea name="body" onChange={this.handleChange} value={this.state.body} />
                </div>
                <br />
                <button type="submit" onClick={this.handleSubmit}>Submit</button>
            </form>
        </div>
        )
    }
}

PostForm.propTypes = {
    newPost: PropTypes.func.isRequired,
    post: PropTypes.array.isRequired
}

//export default PostForm;
export default connect(null,{newPost})(PostForm);