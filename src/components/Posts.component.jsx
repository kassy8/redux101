import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {fetchPosts} from '../actions/post.actions';

class Posts extends Component{
/*
    constructor(props){
        super(props);
        this.state = {
            posts: []
        }
    }
*/
    componentWillMount(){
        this.props.fetchPosts();
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.newPost){
            this.props.posts.unshift(nextProps.newPost);
        }
    }

    render(){
        const postItems= this.props.posts.map((p) => {
            return(<li key={p.id}>
                <h5>{p.title}</h5>
                <p>{p.body}</p>
            </li>);
        });
        return(
            <div>
            <h2>Posts</h2>
            <ul>{postItems}</ul>
            </div>
        )
    }
}

Posts.propTypes = {
    fetchPosts: PropTypes.func.isRequired,
    posts: PropTypes.array.isRequired,
    newPost: PropTypes.object
}

const mapStateToProps = state => ({
    posts: state.posts.items,
    newPost: state.posts.item
});

export default connect(mapStateToProps,{fetchPosts})(Posts);