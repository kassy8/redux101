import {combineReducers} from 'redux';
import postReducer from './postReducer.reducer';

export default combineReducers({
    posts: postReducer
});