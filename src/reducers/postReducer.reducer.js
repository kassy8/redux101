import {FETCH_POSTS, NEW_POST} from '../actions/types';

const initialState = {
    items: [], //the posts that come from the action
    item: {} //the new post we are adding
}

export default function(state=initialState, action){
    switch(action.type){
        case FETCH_POSTS:
            return {
                ...state,
                items: action.payload
            }
            break;
        case NEW_POST:
            //console.log('fetching reducer');
            return {
                ...state,
                item: action.payload
            }
            break;
        default:
            return state;
    }
}